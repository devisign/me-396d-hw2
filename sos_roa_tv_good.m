function [feasible,sol_x,sol_y] = sos_roa_tv_good(rho,xg,ug,dynamics,lqr_sett,tk,tkp1,rho_dot)
    
    echo off;
    m = dynamics.m;
    g = dynamics.g;
    l = dynamics.l;
    b = dynamics.b;
    I = dynamics.I;
    
    S = lqr_sett.S;
    K = lqr_sett.K;
    A = lqr_sett.A;
    B = lqr_sett.B;
    Q = lqr_sett.Q;
    R = lqr_sett.R;
    
    xg1 = xg(1);
    xg2 = xg(2);
    
    % =============================================
    % Defining parameters
    degree      = 3;
    degree_m    = linspace(0,degree,degree+1);
    eps         = 1e-5;
    tol         = 1e-9;

    t           = tk;
    
    % =============================================
    % Defining variables
    syms x1 x2 real
    vars = [x1;x2];
    h1 = monomials([x1,x2],degree_m);    
    h2 = monomials([x1,x2],degree_m);
    h3 = monomials([x1,x2],degree_m);

    % =============================================
    % First, initialize the sum of squares program
    prog = sosprogram(vars);

    % =============================================
    % Define SOS Variables
    [prog,mon1]    = sossosvar(prog,h1);
    [prog,mon2]    = sossosvar(prog,h2);
    [prog,mon3]    = sossosvar(prog,h3);

    % =============================================
    % Next, define constraints
    f           = [...
                    x2;...
                    1/I*(-b*(x2) - (m*g*l)*((x1)*cos(xg1)-(x1)^2/factorial(2)*sin(xg1)-(x1)^3/6*cos(xg1)+(x1)^4/24*sin(xg1)+(x1)^5/120*cos(xg1))-K*(vars))];

    J_star      = vars'*S*vars;
    J_star_dot  = vars'*(-(A'*S + S*A - S*B*inv(R)*B'*S' + Q))*vars + 2*vars'*S*(f);
    constraint  = -eps*((x1)^2+(x2)^2) - mon1*(rho-J_star) - mon2*(t-tk) - mon3*(tkp1-t) - J_star_dot + rho_dot;
    prog        = sosineq(prog,constraint);
    
    % =============================================
    % Solve the problem
    prog = sossolve(prog);
    
    % =============================================
    % Get solution
    primal  = prog.solinfo.info.pinf;
    dual    = prog.solinfo.info.dinf;
    num_err = prog.solinfo.info.numerr;
    if primal == 0 && dual == 0 && num_err == 0
        feasible = 1;
    elseif primal == 0 && dual == 0
        disp('Numerical Errors');
        feasible = 0;
    else
        feasible = 0;
    end
    
end