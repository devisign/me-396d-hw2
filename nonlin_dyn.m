function dx = nonlin_dyn(t,x,u)

    % System parameters:
    b = 0.1;
    m = 1;
    g = 9.81;
    l = 0.5;
    I = m*l^2;
    
    % System   
    q       = x(1,:);
    dq      = x(2,:);
    
    ddq     = (1/I)*(-b*dq-m*g*l*sin(q) + u);
    dx = [dq;ddq];
    
end