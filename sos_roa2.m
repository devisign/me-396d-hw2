function feasible = sos_roa(rho,xg,ug,dynamics,lqr_sett)
    
    echo on;
    m = dynamics.m;
    g = dynamics.g;
    l = dynamics.l;
    b = dynamics.b;
    I = dynamics.I;
    
    S = lqr_sett.S;
    K = lqr_sett.K;
    
    xg1 = xg(1);
    xg2 = xg(2);
    
    % =============================================
    % Defining parameters
    degree      = 3;
    degree_m    = linspace(0,degree,degree+1);
    eps         = 0.01;
    
    % =============================================
    % Defining variables
    pvar x1 x2
    vars = [x1;x2];
    h = monomials([x1-xg1,x2-xg2],degree_m);    
    
    jvars       = vars-xg;
    
    % =============================================
    % Define set
    g2 = jvars'*S*jvars;
    
    % =============================================
    % First, initialize the sum of squares program
    [gbnds,sopt] = pcontain([],g2,h);
    
    % =============================================
    % Define constraints
    
    f            = [...
                    x2-xg2;...
                    1/I*(-b*(x2-xg2) - (m*g*l)*(sin(xg1)+(x1-xg1)*cos(xg1)-(x1-xg1)^2/factorial(2)*sin(xg1)-(x1-xg1)^3/6*cos(xg1)))+ug-K*(vars-xg)];
    J_star       = jvars'*S*jvars;
    J_star_dot   = 2*jvars'*S*f;
    constraint2  = -eps*((x1-xg1)^2+(x2-xg2)^2) - mon*(rho-J_star) - J_star_dot >= 0;
    
    % =============================================
    % Solve the problem
    prog = sossolve(prog);
    
    % =============================================
    % Get solution
    solv = sosgetsol(prog,vars);
    
    
end