function dXdt = mRiccati(t, X, A, B, Q, R)
if Q ~= -1
    X = reshape(X, size(A)); %Convert from "n^2"-by-1 to "n"-by-"n"
    dXdt = A'*X + X*A - X*B*inv(R)*B'*X' + Q; %Determine derivative
    dXdt = dXdt(:); %Convert from "n"-by-"n" to "n^2"-by-1
else
    X = reshape(X, size(A)); %Convert from "n^2"-by-1 to "n"-by-"n"
    dXdt = A'*X + X*A - X*B*inv(R)*B'*X'; %Determine derivative
    dXdt = dXdt(:); %Convert from "n"-by-"n" to "n^2"-by-1
end


% function dXdt = mRiccati(t, X, A, B, R)
% X = reshape(X, size(A)); %Convert from "n^2"-by-1 to "n"-by-"n"
% dXdt = A'*X + X*A - X*B*inv(R)*B'*X'; %Determine derivative
% dXdt = dXdt(:); %Convert from "n"-by-"n" to "n^2"-by-1



% e = eig(X);
% if any(e<=0)
%    pause(0.5);
% end