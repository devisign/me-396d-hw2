function rho = get_roa_tv(xg,ug,dynamics,lqr_sett,tk,tkp1,rhof,rho_dot)

feasible    = 0;

% First ROA for goal point
epsilon     = 0.01;
rho         = rhof;
% rho_top     = rhof;
% rho_bottom  = 0;


while feasible == 0
%     rho = rho_bottom + (rho_top - rho_bottom)/2;
    rho = rho - epsilon;
    feasible = sos_roa_tv_good(rho,xg,ug,dynamics,lqr_sett,tk,tkp1,rho_dot);
    rho
end