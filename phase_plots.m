clear all
close all
clc
echo off

% Physical parameters
m   = 1;
g   = 9.81;
l   = 0.5;
b   = 0.1;%0.1; %0.1
I   = m*l^2;

% Initial conditions
xg  = [pi;0];
ug  = 0;

Q   = diag([10,1]);
R   = 20; 


xgv = -10:0.02:10;%linspace(-3,3,0.01);
ygv = -10:0.02:10;%linspace(-3,3,0.01);
[x1_mesh,x2_mesh] = meshgrid(xgv,ygv);

% Linearized system
A   = [0, 1; m*g*l/I, -b/I];
B   = [0; 1/I];

% lqr 
[K,S] = lqr(A,B,Q,R);

for i = 1:length(x1_mesh(:,1))
    for j = 1:length(x2_mesh(1,:))
        xv  = [x1_mesh(i,j);x2_mesh(i,j)];
        
        % Zero-input linear dynamics
        flin_1(i,j)   = x2_mesh(i,j);
        flin_2(i,j)   = 1/I*(-b*(x2_mesh(i,j)-xg(2)) - (m*g*l)*(sin(xg(1))+(x1_mesh(i,j)-xg(1))*cos(xg(1))-(x1_mesh(i,j)-xg(1))^2/factorial(2)*sin(xg(1))-(x1_mesh(i,j)-xg(1))^3/6*cos(xg(1))));
        flin          = [flin_1(i,j);flin_2(i,j)];
        Vlin_z(i,j)   = 2*(xv-xg)'*S*flin;
        
        % Zero-input nonlinear dynamics
        f_1(i,j)      = x2_mesh(i,j);
        f_2(i,j)      = (1/I)*(-b*x2_mesh(i,j)-m*g*l*sin(x1_mesh(i,j)));
        f             = [f_1(i,j);f_2(i,j)];
        V(i,j)        = 2*(xv-xg)'*S*f;
        
        V_test(i,j)     = 0.5*I*x2_mesh(i,j)^2-m*g*l*cos(x1_mesh(i,j));
        V_dot_test(i,j) = I*x2_mesh(i,j)*(f_2(i,j))+m*g*l*sin(x1_mesh(i,j))*x2_mesh(i,j);
%         V_test(i,j)   = (1-cos(x1_mesh(i,j)))+0.5*(sin(x1_mesh(i,j))+x2_mesh(i,j))^2;
%         V_dot_test(i,j) = 1 + sin(x1_mesh(i,j))*x2_mesh(i,j) + (sin(x1_mesh(i,j))+x2_mesh(i,j))*(cos(x1_mesh(i,j))*x2_mesh(i,j)+f_2(i,j));
%         f_2(i,j)      = (1/I)*(-b*x2_mesh(i,j)-m*g*l*sin(x1_mesh(i,j))+ug-K*xv);        

        % Closed loop linear dynamics
        flin_1_cl(i,j)   = x2_mesh(i,j);        
        flin_2_cl(i,j)   = 1/I*(-b*(x2_mesh(i,j)-xg(2)) - (m*g*l)*(sin(xg(1))+(x1_mesh(i,j)-xg(1))*cos(xg(1))-(x1_mesh(i,j)-xg(1))^2/factorial(2)*sin(xg(1))-(x1_mesh(i,j)-xg(1))^3/6*cos(xg(1)))+ug-K*(xv-xg));        
        flin             = [flin_1_cl(i,j);flin_2_cl(i,j)];
        Vlin_cl(i,j)     = 2*(xv-xg)'*S*flin;
     
        Diff(i,j)     = (xv-xg)'*S*(xv-xg);
        inp_constr(i,j) = norm(K*(xv-xg));
    end
end


roa_idx = find(Diff<2334);
plot(x1_mesh(roa_idx),x2_mesh(roa_idx),'*r');
hold on;

% Test lyap funct
figure();
[C,h] = contour(x1_mesh,x2_mesh,V_dot_test);
clabel(C,h);
hold on;
contour(x1_mesh,x2_mesh,V_dot_test,150);
contour(x1_mesh,x2_mesh,V_dot_test,[0 0]);

% % Closed loop linearized dynamics
[C,h] = contour(x1_mesh,x2_mesh,Vlin_cl);
clabel(C,h);
hold on;
contour(x1_mesh,x2_mesh,Vlin_cl,150);
contour(x1_mesh,x2_mesh,Vlin_cl,[0 0]);
% 
% Zero-input Nonlinear system
[C,h] = contour(x1_mesh,x2_mesh,V);
clabel(C,h);
hold on;
contour(x1_mesh,x2_mesh,V,150);
contour(x1_mesh,x2_mesh,V,[0 0]);
% 
% % Zero-input Linear System
% [C,h] = contour(x1_mesh,x2_mesh,Vlin);
% clabel(C,h);
% hold on;
% contour(x1_mesh,x2_mesh,Vlin,150);
% contour(x1_mesh,x2_mesh,Vlin,[0 0]);


% v0_idx = find(V>-1e-4 & V<1e-4);
% plot(x1_mesh(v0_idx),x2_mesh(v0_idx),'*g');
% 
% ic_idx = find(inp_constr<=3);
% plot(x1_mesh(ic_idx),x2_mesh(ic_idx),'*b');


% LTI Verification
figure();
ball_idx = find(Diff<2334);
plot(Vlin_cl(ball_idx),'r*');

% Positive vdot
figure();
pos_idx = find(Vlin_cl>0);
contour(x1_mesh,x2_mesh,Vlin_cl,150);
hold on;
p = plot(x1_mesh(pos_idx),x2_mesh(pos_idx),'*b','MarkerSize',0.4);


% % Phase portrait and contours closed loop lin dyn
% [cv,ch] = contourf(x1_mesh,x2_mesh,Vlin_cl,150); hold on;
% set(ch,'EdgeColor','none');
% [C,h] = contour(x1_mesh,x2_mesh,Vlin_cl);
% clabel(C,h);
% set(h,'EdgeColor','k');
% set(gcf,'Position',[300,300,800,400])
% xlabel('Angle (rad)');
% ylabel('Angular Velocity (rad/s)');
% h = streamslice(x1_mesh,x2_mesh,flin_1_cl,flin_2_cl);
% title('Phase Portrait and Contours of dV/dt for Closed Loop Linearized Dynamics');
% 
% % Phase portrait and contours nonlinear dyn
% [cv,ch] = contourf(x1_mesh,x2_mesh,V,150); hold on;
% set(ch,'EdgeColor','none');
% [C,h] = contour(x1_mesh,x2_mesh,V);
% clabel(C,h);
% p = contour(x1_mesh,x2_mesh,V,[0 0],'Color','k','LineWidth',2);
% set(p,'LineWidth',1)
% h = streamslice(x1_mesh,x2_mesh,f_1,f_2);
% set(gcf,'Position',[300,300,800,400])
% ylabel('Angular Velocity (rad/s)');
% xlabel('Angle (rad)');
% title('Phase Portrait and Contours of dV/dt for Nonlinear Dynamics');
% 
