function dx = propagation(t,x,timevec,u,dynamics)

u_new = interp1(timevec,u,t);
dx = zeros(2,1);
dx(1) = x(2);
dx(2) = (1/dynamics.I)*(-dynamics.b*x(2)-dynamics.m*dynamics.g*dynamics.l*sin(x(1))+u_new);


% sol = ode45(@(t,x) propagation(t,x,timevec,u,dynamics),[timevec(1),timevec(end)],x0);
% hold on;plot(sol.y(1,:),sol.y(2,:),'*g');