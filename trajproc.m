function rdot=trajproc(t,r,A,c)
rdot=A*r+c;
rdot=rdot(:);
return