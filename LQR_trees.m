clear all
close all
clc
echo off;

% Add shooting method folder to path
addpath('C:\Users\Andrei\Google Drive\Exchange Files\MatthewPeterKelly-OptimTraj-005d957');
addpath('C:\Users\Andrei\Google Drive\Exchange Files\chebfun-master');

% Physical parameters
dynamics.m   = 1;
dynamics.g   = 9.81;
dynamics.l   = 0.5;
dynamics.b   = 0.1; 
dynamics.I   = dynamics.m*dynamics.l^2;

% Initial conditions
xg  = [pi;0];
ug  = 0;

Q   = diag([10,1]);
R   = 20; 
Nf  = 3;
Nt  = 3;

% Linearized system
Ag   = [0, 1; dynamics.m*dynamics.g*dynamics.l/dynamics.I, -dynamics.b/dynamics.I];
Bg   = [0; 1/dynamics.I];

% lqr 
[K,S] = lqr(Ag,Bg,Q,R);
lqr_sett.K = K;
lqr_sett.S = S;

%--------------------------------------------------------------------------
% % ROA
%--------------------------------------------------------------------------
% rho = get_roa(xg,ug,dynamics,lqr_sett);
rho = 8.8;
Ellipse_plot(S,xg,rho);
%--------------------------------------------------------------------------
% Expanding the tree
%--------------------------------------------------------------------------
% Initialize Tree
T.node(1).x = xg(1);
T.node(1).y = xg(2);
T.node(1).parent = 0;
T.node(1).rho = rho;
T.node(1).traj = [];

syms s t

max_nodes = 2;
fails     = 0;

% Grow tree with x nodes
for node_iter = 1:max_nodes
    disp(['node iter: ',num2str(node_iter)]);
    % Sample point
    xk = -2*pi*ones(2,1) + rand(2,1)*4*pi;
    while (xk-xg)'*lqr_sett.S*(xk-xg) <= rho
        xk = -2*pi*ones(2,1) + rand(2,1)*4*pi;
    end
    % xk = [pi/2;pi-pi/3];
    % xk = [pi/2;1];
    if node_iter == 1
        xk = [0;2];
    else
        xk = [0;5.2];
    end

    % linearize around xk and u0 = 0
    f_1     = xk(2);
    f_2     = (1/dynamics.I)*(-dynamics.b*xk(2)-dynamics.m*dynamics.g*dynamics.l*sin(xk(1)));
    A       = [0, 1;...
                 -dynamics.m*dynamics.g*dynamics.l/dynamics.I * cos(xk(1)), -dynamics.b/dynamics.I];
    B       = [0;...
                 1/dynamics.I];
    c       = [f_1;f_2];       

    opt         = odeset('RelTol',1e-6,'AbsTol',1e-6);

    [temp_K,temp_S] = lqr(A,B,zeros(2,2),R);
    temp_P = care(A,B,zeros(2,2),R);
    
    % Finite time LQR to go from all points in the tree to sample 
    for tree_iter = 1:length(T)
        disp(['tree iter: ',num2str(tree_iter)]);
        x_node = [T.node(tree_iter).x;T.node(tree_iter).y];

        % Proceed with finding smallest cost
        clear Jstar tf u_star
        epsilon     = 10;
        iter        = 1;
        tf(iter)    = 0.01;
        Jstar(iter) = 100;
        epsilon_t(iter) = epsilon;

        while epsilon > 0.5 %tf<1
            P0      = eye(2,2);
            [T_P,P] = ode45(@(t,X) mRiccati(t,X,A,B,-1,R),[0,tf(iter)],P0,opt);
            P_tf    = [P(end,1),P(end,3);P(end,2),P(end,4)];

            STM     = double(subs(ilaplace(inv(s*eye(2)-A)),t,tf(iter)));
            K       = inv(R)*B'*STM*inv(P_tf);

            r0          = zeros(2,1);
            [T_r,Rvec]  = ode45(@(t,r) trajproc(t,r,A,c),[0,tf(iter)],r0,opt);
            r           = [Rvec(end,1);Rvec(end,2)];
            d           = r+STM*(x_node-xk);

            iter        = iter +1;
            Jstar(iter) = tf(iter-1)+0.5*d'*inv(P_tf)*d;
            tf(iter)    = tf(iter-1)+0.01;
            u_star(iter) = -K*d;

            epsilon     = abs(norm(P_tf)-norm(temp_P));
            epsilon_t(iter) = epsilon;
        end

        plot(epsilon_t(:),Jstar(:),'*b');
        [Jval,Jind] = min(Jstar);

        min_time(tree_iter) = tf(Jind);
        min_cost(tree_iter) = Jval;
        u_star(tree_iter)   = u_star(Jind);

    end
    [~,ind] = min(min_cost);
    tf      = min_time(ind);
    u_star  = u_star(ind);

    % Using Trajectory Optimization package
    problem.func.dynamics = @(t,x,u) nonlin_dyn(t,x,u);
    problem.func.pathObj = @(t,x,u)( 1 + u.^2.*R );

    % Problem bounds
    problem.bounds.initialTime.low = 0;
    problem.bounds.initialTime.upp = 0;
    problem.bounds.finalTime.low = tf-1;
    problem.bounds.finalTime.upp = tf+1;

    problem.bounds.state.low = [-2*pi; -inf];
    problem.bounds.state.upp = [2*pi; inf];
    problem.bounds.initialState.low = [x_node(1);x_node(2)];
    problem.bounds.initialState.upp = [x_node(1);x_node(2)];
    problem.bounds.finalState.low = [xk(1);xk(2)];
    problem.bounds.finalState.upp = [xk(1);xk(2)];

    problem.bounds.control.low = -3; %-inf;
    problem.bounds.control.upp = 3; %inf;

    % Guess at the initial trajectory
    problem.guess.time = [0,tf];
    problem.guess.state = [x_node(1), x_node(2); xk(1), xk(2)];
    problem.guess.control = [u_star, 0];

    % Select a solver:
%     problem.options.method = 'chebyshev';
    % problem.options.method = 'hermiteSimpson';
%     problem.options.method = 'trapezoid';
    problem.options.method = 'rungeKutta';
    problem.options.defaultAccuracy = 'medium';
    problem.options.MaxIterations = 1000;
    problem.options.trapezoid.nGrid = 80;
    problem.options.rungeKutta.nSegment = 50;
    problem.options.chebyshev.nColPts = 40;

    % Solve the problem
    soln = optimTraj(problem);
    timevec = soln.grid.time;
    q = soln.grid.state(1,:);
    dq = soln.grid.state(2,:);
    u = soln.grid.control;
    
    if soln.info.exitFlag ~= 1
        fails = fails + 1;
        continue
    end

    % Animate the results:
    % z = [q;dq];
    % Draw.plotFunc = @(t,z)( drawPendulum(t,z,dynamics) );
    % Draw.speed = 0.23;
    % Draw.figNum = 1000;
    % animate(timevec,z,Draw)

    %--------------------------------------------------------------------------
    % Time varying LQR for tube 
    %--------------------------------------------------------------------------
    clear traj
    fixed_tf    = timevec(end);
    d_traj      = 1;
    rho_fail    = 0;
    for trajiter=1:d_traj:length(q)-1
        
        disp(['Iteration ',num2str(trajiter),' for TV-LQR, out of ',num2str(length(q))]);
        
        if trajiter == 1
            traj(trajiter).rho = T.node(ind).rho;
            rho_dot = 0;
        else
            % linearize around current point current control
            f_1      = dq(trajiter);
            f_2      = (1/dynamics.I)*(-dynamics.b*dq(trajiter)-dynamics.m*dynamics.g*dynamics.l*sin(q(trajiter)));
            At       = [0, 1;...
                         -dynamics.m*dynamics.g*dynamics.l/dynamics.I * cos(q(trajiter)), -dynamics.b/dynamics.I];
            Bt       = [0;... 
                         1/dynamics.I];
            Q        = diag([10,1]);
            R        = 20;

            [K_tf,S_tf]    = lqr(Ag,Bg,Q,R);
            if timevec(trajiter)~=0
                [T_St,St] = ode45(@(t,X) mRiccatibackwards(t,X,Q,R,timevec,q,dynamics),[0,timevec(trajiter)],S_tf);
                traj(trajiter).S_t     = reshape(St(end,1:4),[2,2]);
            else
                St = S_tf;
                traj(trajiter).S_t     = St;
            end

            traj(trajiter).K_t     = inv(R)*Bt'*traj(trajiter).S_t

            lqr_tv_sett.K = traj(trajiter).K_t;
            lqr_tv_sett.S = traj(trajiter).S_t;
            lqr_tv_sett.A = At;
            lqr_tv_sett.B = Bt;
            lqr_tv_sett.Q = Q;
            lqr_tv_sett.R = R;

            traj(trajiter).rho = get_roa_tv([q(trajiter),dq(trajiter)],u(trajiter),dynamics,lqr_tv_sett,fixed_tf - timevec(trajiter),fixed_tf - timevec(trajiter+d_traj), traj(trajiter-d_traj).rho, rho_dot);           
            rho_dot = (traj(trajiter-d_traj).rho - traj(trajiter).rho)/(timevec(trajiter)-timevec(trajiter-d_traj));
            
            if traj(end).rho <= 0
                rho_fail = 1;
                break
            end
        end
        traj = traj(1:d_traj:end);
        
    end
    
    if rho_fail == 1
        fails = fails + 1;
        continue
    end
    
    for trajiter = 1:length(traj)
        Ellipse_plot_time(traj(trajiter).S_t,[q(trajiter),dq(trajiter)],traj(trajiter).rho,timevec(trajiter));
    end
    h = plot3(q,dq,timevec,'b-',x_node(1),x_node(2),timevec(1),'r*',xk(1),xk(2),timevec(end),'*g'); 
    pause(0.1);
    hold on;
    
    T.node(node_iter+1).x = xk(1);
    T.node(node_iter+1).y = xk(2);
    T.node(node_iter+1).parent = ind;
    T.node(node_iter+1).rho = traj(end).rho;
    T.node(node_iter+1).traj = traj;
    
end