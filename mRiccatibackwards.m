function dXdt = mRiccatibackwards(t, X, Q, R,timevec,q,dynamics)

xs       = interp1(timevec,q,t);

A        = [0, 1;...
                 -dynamics.m*dynamics.g*dynamics.l/dynamics.I * cos(xs), -dynamics.b/dynamics.I];
B        = [0;... 
             1/dynamics.I];

X = reshape(X, size(A)); %Convert from "n^2"-by-1 to "n"-by-"n"
dXdt = (A'*X + X*A - X*B*inv(R)*B'*X' + Q); %Determine derivative
dXdt = dXdt(:); %Convert from "n"-by-"n" to "n^2"-by-1