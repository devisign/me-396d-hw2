function feasible = sos_roa_yalmip(rho,xg,ug,dynamics,lqr_sett)
    

    echo on;
    m = dynamics.m;
    g = dynamics.g;
    l = dynamics.l;
    b = dynamics.b;
    I = dynamics.I;
    
    S = lqr_sett.S;
    K = lqr_sett.K;
    
    xg1 = xg(1);
    xg2 = xg(2);
    
    % =============================================
    % Defining parameters
    degree      = 8;
    %degree_m    = linspace(0,degree,degree+1);
    eps         = 1e-5;
    tol         = 1e-9;
    
    % =============================================
    % Defining variables
    %syms x1 x2 real
    
    xx = sdpvar(2,1);
    
    %vars = [x1;x2];
    %h = monomials([x1-xg1,x2-xg2],degree_m);    
    
    [h,h_coeffs] = polynomial(xx,degree);
    
    % =============================================
    % First, initialize the sum of squares program
    %prog = sosprogram(vars);
    
    % =============================================
    % Define SOS Variables
    %[prog,mon]    = sossosvar(prog,h);
    
    % =============================================
    % Next, define constraints
    f           = [xx(2); 1/I*(-b*(xx(2)) - (m*g*l)*(sin(xg1)+(xx(1))*cos(xg1)-(xx(1))^2/factorial(2)*sin(xg1)-(xx(1))^3/6*cos(xg1))+ug-K*(xx))];
    %jvars       = vars-xg;
    J_star      = xx'*S*xx;
    J_star_dot  = 2*xx'*S*f;
    constraint  = -eps*((xx(1))^2+(xx(2))^2) - h*(rho-J_star) - J_star_dot;
    %prog        = sosineq(prog,constraint);
    
    % =============================================
    % Solve the problem
    %prog = sossolve(prog);
    
    % =============================================
    % Get solution
    %solv = sosgetsol(prog,vars);
    
%     options = sdpsettings('sos.model',1,'solver','mosek','verbose',1,'mosek.MSK_DPAR_INTPNT_CO_TOL_DFEAS',1e-14,'mosek.MSK_DPAR_INTPNT_CO_TOL_INFEAS',1e-14,'mosek.MSK_DPAR_INTPNT_CO_TOL_PFEAS',1e-14,'mosek.MSK_DPAR_INTPNT_CO_TOL_DFEAS',1e-14);%,'sos.numblk',1e-8);
    options = sdpsettings('sos.model',1,'solver','sedumi','verbose',1);
    
%     options = sdpsettings('sos.model',1,'solver','mosek','verbose',1,'mosek.MSK_DPAR_INTPNT_CO_TOL_DFEAS',1e-8,'mosek.MSK_DPAR_INTPNT_CO_TOL_INFEAS',1e-8,'mosek.MSK_DPAR_INTPNT_CO_TOL_PFEAS',1e-9,'mosek.MSK_DPAR_INTPNT_CO_TOL_DFEAS',1e-8,'mosek.MSK_DPAR_DATA_TOL_X',1e-9,'sos.sparsetol',1e-10,'sos.postprocess',1,'sos.newton',1,'sos.inconsistent',1);

    
    sol = solvesos(sos(constraint),[],options,h_coeffs)

    if sol.problem == 0
        feasible = 1;
    else
        feasible = 0;
    end
end