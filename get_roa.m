function rho = get_roa(xg,ug,dynamics,lqr_sett)

feasible    = 1;
rho         = 8;

% First ROA for goal point
epsilon     = 10;
rho_top     = 5000;
rho_bottom  = 0;
while rho_top-rho_bottom > epsilon
    rho = rho_bottom + (rho_top - rho_bottom)/2;
    feasible = sos_roa(rho,xg,ug,dynamics,lqr_sett);
    if feasible == 0
        rho_top = rho;
    else
        rho_bottom = rho;
    end
    rho
end
